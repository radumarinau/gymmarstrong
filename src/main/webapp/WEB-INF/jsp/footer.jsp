<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>
	<script
        src="<c:url value="https://code.jquery.com/jquery-3.3.1.slim.min.js"/>"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script
        src="<c:url value="/resources/js/assets/js/vendor/popper.min.js"/>"></script>
    <script src="<c:url value="/resources/js/dist/js/bootstrap.min.js"/>"></script>
</body>
</html>