<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Class Types</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<a href="${pageContext.request.contextPath}/classType/save">Add
				new gym class type</a> <br />
			<br />

			<h2>All gym class types</h2>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Update</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${classTypeList}" var="classType">
						<tr>
							<td>${classType.classTypeName}</td>
							<td>${classType.classTypeDescription}</td>
							<td>
								<form name='UpdateClassType' method='POST'
									action='/GymMarStrong/classType/update'>
									<input type="hidden" name="classTypeId"
										value="${classType.classTypeId}" /> <input type="submit"
										value="Update" />
								</form>
							</td>
							<td>
								<form name='DeleteClassType' method='POST'
									action='/GymMarStrong/classType/delete'>
									<input type="hidden" name="classTypeId"
										value="${classType.classTypeId}" /> <input type="submit"
										value="Delete" />
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>