<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Trainers</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<a href="${pageContext.request.contextPath}/trainer/save">Add new trainer</a>
            <br/><br/>

			<h2>All trainers</h2>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>First name</th>
						<th>Last name</th>
						<th>CNP</th>
						<th>GymClasses</th>
						<th>Update</th>
						<th>Delete</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach items="${trainerList}" var="trainer">
						<tr>
							<td>${trainer.firstName}</td>
							<td>${trainer.lastName}</td>
							<td>${trainer.cnp}</td>
							<td>
								<table>
									<c:forEach items="${trainer.gymClassList}" var="gymClass">
										<tr>
											<td>${gymClass.classType.classTypeName }</td>
											<td>${gymClass.startDateTime }</td>
											<td>${gymClass.endDateTime }</td>
										</tr>
									</c:forEach>
								</table>
							</td>
							<td>
								<form name='UpdateTrainer' method='post'
									action='/GymMarStrong/trainer/update'>
									<input type="hidden" name="trainerId"
										value="${trainer.trainerId}" /> <input type="submit"
										value="Update" />
								</form>
							</td>
							<td>
								<form name='DeleteTrainer' method='post'
									action='/GymMarStrong/trainer/delete'>
									<input type="hidden" name="trainerId"
										value="${trainer.trainerId}" /> <input type="submit"
										value="Delete" />
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>