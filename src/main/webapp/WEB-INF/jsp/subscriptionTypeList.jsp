<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Class Types</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<a href="${pageContext.request.contextPath}/subscriptionType/save">Add
				new subscription type</a> <br />
			<br />

			<h2>All subscription types</h2>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>Access time</th>
						<th>Services</th>
						<th>Update</th>
						<th>Delete</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach items="${subscriptionTypeList}" var="subscriptionType">
						<tr>
							<td>${subscriptionType.accessTime}</td>
							<td>${subscriptionType.services}</td>
							<td>
								<form name='UpdateSubscriptionType' method='post'
									action='/GymMarStrong/subscriptionType/update'>
									<input type="hidden" name="subscriptionTypeId"
										value="${subscriptionType.subscriptionTypeId}" /> <input
										type="submit" value="Update" />
								</form>
							</td>
							<td>
								<form name='DeleteSubscriptionType' method='post'
									action='/GymMarStrong/subscriptionType/delete'>
									<input type="hidden" name="subscriptionTypeId"
										value="${subscriptionType.subscriptionTypeId}" /> <input
										type="submit" value="Delete" />
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>