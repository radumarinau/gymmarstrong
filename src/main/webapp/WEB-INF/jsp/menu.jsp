<html>
<body>
	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
		<a class="navbar-brand" href="${pageContext.request.contextPath}/customer">GymMarStrong</a>

		<div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="${pageContext.request.contextPath}/customer">Customers <span
						class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link"
					href="${pageContext.request.contextPath}/trainer">Trainers</a></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" id="dropdown01"
					data-toggle="dropdown" href="" aria-haspopup="true"
					aria-expanded="false">Subscriptions</a>
					<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item"
							href="${pageContext.request.contextPath}/subscription">List</a> <a
							class="dropdown-item"
							href="${pageContext.request.contextPath}/subscriptionType">Types</a>
					</div></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" id="dropdown01"
					data-toggle="dropdown" href="" aria-haspopup="true"
					aria-expanded="false">Classes</a>
					<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item"
							href="${pageContext.request.contextPath}/gymClass">List</a> <a
							class="dropdown-item"
							href="${pageContext.request.contextPath}/classType">Types</a>
					</div></li>
			</ul>
		</div>
	</nav>
</body>
</html>