<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<title>All subscriptions</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<a href="${pageContext.request.contextPath}/subscription/save">Add new subscription</a>
            <br/><br/>

			<h2>All subscriptions</h2>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>Subscription Type</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Price</th>
						<th>Owner</th>
						<th>Update</th>
						<th>Delete</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach items="${subscriptionList}" var="subscription">
						<tr>
							<td>${subscription.subscriptionType.services}</td>
							<td><fmt:formatDate type = "date" value="${subscription.startDateTime}"/></td>
							<td><fmt:formatDate type = "date" value="${subscription.endDateTime}"/></td>
							<td>${subscription.price}</td>
							<td>${subscription.customer.fullName }</td>
							<td>
								<form name='UpdateSubscription' method='post'
									action='/GymMarStrong/subscription/update'>
									<input type="hidden" name="subscriptionId"
										value="${subscription.subscriptionId}" /> <input
										type="submit" value="Update" />
								</form>
							</td>
							<td>
								<form name='DeleteSubscription' method='post'
									action='/GymMarStrong/subscription/delete'>
									<input type="hidden" name="subscriptionId"
										value="${subscription.subscriptionId}" /> <input
										type="submit" value="Delete" />
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</div>
	</div>
	
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>