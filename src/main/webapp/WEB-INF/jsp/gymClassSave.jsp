<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Gym Classes</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<h2>Save gym class</h2>
			<form:form name='SaveGymClass' modelAttribute="newGymClass"
				method='post' action='/GymMarStrong/gymClass/save'
				class="form-horizontal">
				<form:input type="hidden" path="gymClassId" />

				<div class="form-group">
					<form:label path="startDateTime" class="control-label col-sm-2">Start Date</form:label>
					<div class="col-sm-5">
						<form:input path="startDateTime" type="date" class="form-control" />
					</div>
				</div>

				<div class="form-group">
					<form:label path="endDateTime" class="control-label col-sm-2">End Date</form:label>
					<div class="col-sm-5">
						<form:input path="endDateTime" type="date" class="form-control" />
					</div>
				</div>

				<div class="form-group">
					<form:label path="maxParticipants" class="control-label col-sm-5">Max nr of participants</form:label>
					<div class="col-sm-5">
						<form:input path="maxParticipants" class="form-control" />
					</div>
				</div>

				<div class="form-group">
					<form:label path="trainer" class="control-label col-sm-5">Trainer</form:label>
					<div class="col-sm-5">
					<form:select path="trainer.trainerId" class="form-control col-sm-4">
						<form:options items="${trainerList }" itemValue="trainerId"
							itemLabel="fullName" />
					</form:select>
					</div>
				</div>

				<div class="form-group">
					<form:label path="classType" class="control-label col-sm-5">Class type</form:label>
					<div class="col-sm-5">
					<form:select path="classType.classTypeId"
						class="form-control col-sm-4">
						<form:options items="${classTypeList }" itemValue="classTypeId"
							itemLabel="classTypeName" />
					</form:select>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-5">
						<input type="submit" value="Save" class="btn btn-default" />
					</div>
				</div>
			</form:form>

			<a href="${pageContext.request.contextPath}/gymClass">Back</a>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>