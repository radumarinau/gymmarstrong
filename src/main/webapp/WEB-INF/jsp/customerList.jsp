<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<title>Customers</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

            <a href="${pageContext.request.contextPath}/customer/save">Add new customer</a>
            <br/><br/>

			<h2>All customers</h2>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>First name</th>
						<th>Last name</th>
						<th>CNP</th>
						<th>Subscriptions</th>
						<th>Classes</th>
						<th>Update</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach items="${customerList}" var="customer">
						<tr>
							<td>${customer.firstName}</td>
							<td>${customer.lastName}</td>
							<td>${customer.cnp}</td>
							<td>
								<table>
									<c:forEach items="${customer.subscriptionList}"
										var="subscription">
										<tr>
											<td>${subscription.subscriptionType.services }</td>
											<td>${subscription.subscriptionType.accessTime }</td>
											<td><fmt:formatDate type = "date" value="${subscription.startDateTime}"/></td>
											<td><fmt:formatDate type = "date" value="${subscription.endDateTime}"/></td>
										</tr>
									</c:forEach>
								</table>
							</td>
							<td>
								<table>
									<c:forEach items="${customer.gymClassList }" var="gymClass">
										<tr>
											<td>${gymClass.classType}
										</tr>
									</c:forEach>
								</table>
							<td>
								<form name='UpdateCustomer' method='post'
									action='/GymMarStrong/customer/update'>
									<input type="hidden" name="customerId"
										value="${customer.customerId}" /> <input type="submit"
										value="Update" />
								</form>
							</td>
							<td>
								<form name='DeleteCustomer' method='post'
									action='/GymMarStrong/customer/delete'>
									<input type="hidden" name="customerId"
										value="${customer.customerId}" /> <input type="submit"
										value="Delete" />
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>