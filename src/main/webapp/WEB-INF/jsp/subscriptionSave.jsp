<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Subscriptions</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
    rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
    rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<h2>Save subscription</h2>
			<form:form name='SaveSubscription' modelAttribute="newSubscription"
				method='post' action='/GymMarStrong/subscription/save'
				class="form-horizontal">
				<form:input type="hidden" path="subscriptionId" />

				<div class="form-group">
					<form:label path="startDateTime" class="control-label col-sm-2">Start Date</form:label>
					<div class="col-sm-5">
						<form:input path="startDateTime" type="date" class="form-control" required="true"/>
					</div>
				</div>

				<div class="form-group">
					<form:label path="endDateTime" class="control-label col-sm-2">End Date</form:label>
					<div class="col-sm-5">
						<form:input path="endDateTime" type="date" class="form-control" required="true"/>
					</div>
				</div>

				<div class="form-group">
					<form:label path="price" class="control-label col-sm-2">Price</form:label>
					<div class="col-sm-5">
						<form:input path="price" class="form-control" required="true"/>
					</div>
				</div>

				<div class="form-group">
					<form:label path="subscriptionType" class="control-label col-sm-5">Subscription type</form:label>
					<form:select path="subscriptionType.subscriptionTypeId"
						class="form-control col-sm-4">
						<form:options items="${subscriptionTypeList}"
							itemValue="subscriptionTypeId" itemLabel="services" />
					</form:select>
				</div>

				<div class="form-group">
					<form:label path="customer" class="control-label col-sm-5">Customer</form:label>
					<form:select path="customer.customerId"
						class="form-control col-sm-4">
						<form:options items="${customerList}" itemValue="customerId"
							itemLabel="fullName" />
					</form:select>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-5">
						<input type="submit" value="Save" class="btn btn-default" />
					</div>
				</div>
			</form:form>
			
			<a href="${pageContext.request.contextPath}/subscription">Back</a>
		</div>
	</div>

	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>