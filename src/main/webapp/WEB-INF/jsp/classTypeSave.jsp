<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Class Types</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<h2>Save class type</h2>
			<form:form name='SaveClassType' modelAttribute="newClassType"
				method='post' action='/GymMarStrong/classType/save'
				class="form-horizontal">
				<form:input type="hidden" path="classTypeId" />

				<div class="form-group">
					<form:label path="classTypeName" class="control-label col-sm-2">Name</form:label>
					<div class="col-sm-5">
						<form:input path="classTypeName" class="form-control" required="true" />
					</div>
				</div>

				<div class="form-group">
					<form:label path="classTypeDescription"
						class="control-label col-sm-2">Description</form:label>
					<div class="col-sm-5">
						<form:input path="classTypeDescription" class="form-control" required="true"/>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-5">
						<input type="submit" value="Save" class="btn btn-default" />
					</div>
				</div>
			</form:form>
			
			<a href="${pageContext.request.contextPath}/classType">Back</a>
		</div>
	</div>


	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>