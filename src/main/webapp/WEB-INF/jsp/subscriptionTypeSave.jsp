<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Subscription Types</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<h2>Save subscription type</h2>
			<form:form name='SaveSubscriptionType'
				modelAttribute="newSubscriptionType" method='post'
				action='/GymMarStrong/subscriptionType/save' class="form-horizontal">
				<form:input type="hidden" path="subscriptionTypeId" />

				<div class="form-group">
					<form:label path="accessTime" class="control-label col-sm-2">Access Time</form:label>
					<div class="col-sm-5">
						<form:input path="accessTime" required="true"/>
					</div>
				</div>

				<div class="form-group">
					<form:label path="services" class="control-label col-sm-2">Services</form:label>
					<div class="col-sm-5">
						<form:input path="services" required="true"/>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-5">
						<input type="submit" value="Save" class="btn btn-default" />
					</div>
				</div>
			</form:form>
			
			<a href="${pageContext.request.contextPath}/subscriptionType">Back</a>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>