<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Trainers</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<h2>Save trainer</h2>
			<form:form name='SaveTrainer' modelAttribute="newTrainer"
				method='post' action='/GymMarStrong/trainer/save'
				class="form-horizontal">
				<form:input type="hidden" path="trainerId" />

				<div class="form-group">
					<form:label path="firstName" class="control-label col-sm-2">First Name</form:label>
					<div class="col-sm-5">
						<form:input path="firstName" required="true" pattern="[a-zA-Z]+" />
					</div>
				</div>

				<div class="form-group">
					<form:label path="lastName" class="control-label col-sm-2">Last Name</form:label>
					<div class="col-sm-5">
						<form:input path="lastName" required="true" pattern="[a-zA-Z]+" />
					</div>
				</div>

				<div class="form-group">
					<form:label path="cnp" class="control-label col-sm-2">CNP</form:label>
					<div class="col-sm-5">
						<form:input path="cnp" required="true" pattern="[0-9]{13}" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-5">
						<input type="submit" value="Save" class="btn btn-default" />
					</div>
				</div>
			</form:form>

			<a href="${pageContext.request.contextPath}/trainer">Back</a>
		</div>
	</div>


	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>