<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
<title>All classes</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<a href="${pageContext.request.contextPath}/gymClass/save">Add
				new gym class</a> <br /> <br />


			<h2>All classes</h2>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>Class Type</th>
						<th>Start Date</th>
						<th>End Date</th>
						<th>Trainer</th>
						<th>Participants
						<th>Update</th>
						<th>Delete</th>
						<th>Update Participants</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach items="${gymClassList}" var="gymClass">
						<tr>
							<td>${gymClass.classType.classTypeName}</td>
							<td><fmt:formatDate type = "date" value="${gymClass.startDateTime}"/></td>
							<td><fmt:formatDate type = "date" value="${gymClass.endDateTime}"/></td>
							<td>${gymClass.trainer.fullName}</td>
							<td>${gymClass.numberOfClassParticiapants}/${gymClass.maxParticipants}</td>
							<td>
								<form name='UpdateGymClass' method='post'
									action='/GymMarStrong/gymClass/update'>
									<input type="hidden" name="gymClassId"
										value="${gymClass.gymClassId}" /> <input type="submit"
										value="Update" />
								</form>
							</td>
							<td>
								<form name='DeleteGymClass' method='post'
									action='/GymMarStrong/gymClass/delete'>
									<input type="hidden" name="gymClassId"
										value="${gymClass.gymClassId}" /> <input type="submit"
										value="Delete" />
								</form>
							</td>
							<td>
								<form name='ParticipantsManagement' method='post'
									action='/GymMarStrong/gymClass/updateParticipants'>
									<input type="hidden" name="gymClassId"
										value="${gymClass.gymClassId}" /> <input type="submit"
										value="Update Participants" />
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>