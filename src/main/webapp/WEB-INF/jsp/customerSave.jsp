<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Customers</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<h2>Save customer</h2>
			<form:form name='SaveCustomer' modelAttribute="newCustomer"
				method='post' action='/GymMarStrong/customer/save'
				class="form-horizontal">
				<form:input type="hidden" path="customerId" />

				<div class="form-group">
					<form:label path="firstName" class="control-label col-sm-2">First Name</form:label>
					<div class="col-sm-5">
						<form:input path="firstName" class="form-control" required="true"
							pattern="[a-zA-Z]+" />
					</div>
				</div>

				<div class="form-group">
					<form:label path="lastName" class="control-label col-sm-2">Last Name</form:label>
					<div class="col-sm-5">
						<form:input path="lastName" class="form-control" required="true"
							pattern="[a-zA-Z]+" />
					</div>
				</div>

				<div class="form-group">
					<form:label path="cnp" class="control-label col-sm-2">CNP</form:label>
					<div class="col-sm-5">
						<form:input path="cnp" class="form-control" required="true"
							pattern="[0-9]{13}" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-5">
						<input type="submit" value="Save" class="btn btn-default" />
					</div>
				</div>
			</form:form>

			<c:if test="${not empty newCustomer.customerId}">
				<c:if test="${not empty newCustomer.gymClassList}">
					<h2>Remove customer from class</h2>
					<table class="table table-striped table-sm">
						<thead>
							<tr>
								<th>Class Name</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${newCustomer.gymClassList}" var="gymClass">
								<tr>
									<td>${gymClass.classType.classTypeName}</td>
									<td><form:form name='removeClassFromCustomer'
											method='post'
											action='/GymMarStrong/customer/removeCustomerClass'>
											<input type="hidden" name="customerId"
												value="${newCustomer.customerId}" />
											<input type="hidden" name="gymClassId"
												value="${gymClass.gymClassId}" />
											<input type="submit" value="Remove" />
										</form:form></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</c:if>

			<a href="${pageContext.request.contextPath}/customer">Back</a>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>