<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<title>Customers</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/jumbotron.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="/WEB-INF/jsp/menu.jsp" />

	<div class="jumbotron">
		<div class="container">

			<h2>All customers enrolled</h2>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th>First name</th>
						<th>Last name</th>
						<th>CNP</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${enrolledCustomersOfClass}" var="customer">
						<tr>
							<td>${customer.firstName}</td>
							<td>${customer.lastName}</td>
							<td>${customer.cnp}</td>
							<td>
								<form name='RemoveCustomerFromClass' method='post'
									action='/GymMarStrong/gymClass/removeCustomerFromClass'>
									<input type="hidden" name="customerId"
										value="${customer.customerId}" /> <input type="hidden"
										name="gymClassId" value="${gymClassId}" /> <input
										type="submit" value="Remove" />
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div>
			<form name='AddCustomerToClass' method='post'
						action='/GymMarStrong/gymClass/addCustomerToClass'>
						<select name="customerId" class="form-control col-sm-4">
							<c:forEach items="${customersNotEnrolledInClass}" var="customer">
								<option value="${customer.customerId}">
									${customer.credentials}</option>
							</c:forEach>
						</select> <input type="hidden" name="gymClassId" value="${gymClassId}" />
						<input type="submit" value="Add" />
					</form>
			</div>
			<a href="${pageContext.request.contextPath}/gymClass">Back</a>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/footer.jsp" />
</body>
</html>