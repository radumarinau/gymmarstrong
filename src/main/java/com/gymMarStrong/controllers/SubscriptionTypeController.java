package com.gymMarStrong.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gymMarStrong.hibernate.entityClasses.SubscriptionType;
import com.gymMarStrong.service.SubscriptionTypeService;

@Controller
@RequestMapping("/subscriptionType")
public class SubscriptionTypeController {

	@Autowired
	private SubscriptionTypeService subscriptionTypeService;

	@GetMapping
	public String viewAllSubscriptionTypes(ModelMap model) {
		Set<SubscriptionType> subscriptionTypeList = subscriptionTypeService.getAllSubscriptionTypes();
		model.addAttribute("subscriptionTypeList", subscriptionTypeList);
		model.addAttribute("newSubscriptionType", new SubscriptionType());
		return "subscriptionTypeList";
	}

	@GetMapping(value = "/save")
	public String addNewSubscriptionType(ModelMap model) {
		model.addAttribute("newSubscriptionType", new SubscriptionType());
		return "subscriptionTypeSave";
	}

	@PostMapping(value = "/save")
	public String saveSubscriptionType(@ModelAttribute("newSubscriptionType") SubscriptionType subscriptionType,
			ModelMap model) {
		subscriptionTypeService.saveSubscriptionType(subscriptionType);
		Set<SubscriptionType> subscriptionTypeList = subscriptionTypeService.getAllSubscriptionTypes();
		model.addAttribute("subscriptionTypeList", subscriptionTypeList);
		model.addAttribute("newSubscriptionType", new SubscriptionType());
		return "subscriptionTypeList";
	}

	@PostMapping(value = "/update")
	public String updateSubscriptionType(@RequestParam(value = "subscriptionTypeId") String subscriptionTypeId,
			ModelMap model) {
		model.addAttribute("newSubscriptionType",
				subscriptionTypeService.getSubscriptionTypeById(Integer.valueOf(subscriptionTypeId)));
		return "subscriptionTypeSave";
	}

	@PostMapping(value = "/delete")
	public String deleteSubscriptionType(@RequestParam(value = "subscriptionTypeId") String subscriptionTypeId,
			ModelMap model) {
		subscriptionTypeService.deleteSubscriptionTypeById(Integer.valueOf(subscriptionTypeId));
		Set<SubscriptionType> subscriptionTypeList = subscriptionTypeService.getAllSubscriptionTypes();
		model.addAttribute("subscriptionTypeList", subscriptionTypeList);
		model.addAttribute("newSubscriptionType", new SubscriptionType());
		return "subscriptionTypeList";
	}

}
