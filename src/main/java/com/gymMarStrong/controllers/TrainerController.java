package com.gymMarStrong.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gymMarStrong.hibernate.entityClasses.GymClass;
import com.gymMarStrong.hibernate.entityClasses.Trainer;
import com.gymMarStrong.service.GymClassService;
import com.gymMarStrong.service.TrainerService;

@Controller
@RequestMapping("/trainer")
public class TrainerController {

	@Autowired
	private TrainerService trainerService;
	@Autowired
	private GymClassService gymClassService;

	@GetMapping
	public String viewAllTrainers(ModelMap model) {
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		Set<Trainer> trainerList = trainerService.getAllTrainers();
		model.addAttribute("trainerList", trainerList);
		model.addAttribute("newTrainer", new Trainer());
		model.addAttribute("gymClassList", gymClassList);
		return "trainerList";
	}

	@GetMapping(value = "/save")
	public String addNewTrainer(ModelMap model) {
		model.addAttribute("newTrainer", new Trainer());
		return "trainerSave";
	}

	@PostMapping(value = "/save")
	public String saveTrainer(@ModelAttribute("newTrainer") Trainer newTrainer, ModelMap model) {
		trainerService.saveTrainer(newTrainer);
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		Set<Trainer> trainerList = trainerService.getAllTrainers();
		model.addAttribute("gymClassList", gymClassList);
		model.addAttribute("trainerList", trainerList);
		model.addAttribute("newTrainer", new Trainer());
		return "trainerList";
	}

	@PostMapping(value = "/update")
	public String updateTrainer(@RequestParam("trainerId") String trainerId, ModelMap model) {
		Trainer trainer = trainerService.getTrainerById(Integer.valueOf(trainerId));
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		Set<Trainer> trainerList = trainerService.getAllTrainers();
		model.addAttribute("gymClassList", gymClassList);
		model.addAttribute("trainerList", trainerList);
		model.addAttribute("newTrainer", trainer);
		return "trainerSave";
	}

	@PostMapping(value = "/delete")
	public String deleteTrainer(@RequestParam("trainerId") String trainerId, ModelMap model) {
		trainerService.deleteTrainerById(Integer.valueOf(trainerId));
		Set<Trainer> trainerList = trainerService.getAllTrainers();
		model.addAttribute("trainerList", trainerList);
		model.addAttribute("newTrainer", new Trainer());
		return "trainerList";
	}
}
