package com.gymMarStrong.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gymMarStrong.hibernate.entityClasses.ClassType;
import com.gymMarStrong.service.ClassTypeService;

@Controller
@RequestMapping("/classType")
public class ClassTypeController {

	@Autowired
	private ClassTypeService classTypeService;

	@GetMapping
	public String viewAllClassTypes(ModelMap model) {
		Set<ClassType> classTypeList = classTypeService.getAllClassTypes();
		model.addAttribute("classTypeList", classTypeList);
		return "classTypeList";
	}

	@GetMapping(value = "/save")
	public String addNewClassType(ModelMap model) {
		model.addAttribute("newClassType", new ClassType());
		return "classTypeSave";
	}

	@PostMapping(value = "/update")
	public String updateClassType(@RequestParam(value = "classTypeId") String classTypeId, ModelMap model) {
		model.addAttribute("newClassType", classTypeService.getClassTypeById(Integer.valueOf(classTypeId)));
		return "classTypeSave";
	}

	@PostMapping(value = "/save")
	public String saveClassType(@ModelAttribute("newClassType") ClassType classType, ModelMap model) {
		classTypeService.saveClassType(classType);
		Set<ClassType> classTypeList = classTypeService.getAllClassTypes();
		model.addAttribute("classTypeList", classTypeList);
		model.addAttribute("newClassType", new ClassType());
		return "classTypeList";
	}

	@PostMapping(value = "/delete")
	public String deleteClassType(@RequestParam(value = "classTypeId") String classTypeId, ModelMap model) {
		classTypeService.deleteClassTypeById(Integer.valueOf(classTypeId));
		Set<ClassType> classTypeList = classTypeService.getAllClassTypes();
		model.addAttribute("classTypeList", classTypeList);
		model.addAttribute("newClassType", new ClassType());
		return "classTypeList";
	}
}
