package com.gymMarStrong.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gymMarStrong.hibernate.entityClasses.ClassType;
import com.gymMarStrong.hibernate.entityClasses.Customer;
import com.gymMarStrong.hibernate.entityClasses.GymClass;
import com.gymMarStrong.hibernate.entityClasses.Trainer;
import com.gymMarStrong.service.ClassTypeService;
import com.gymMarStrong.service.CustomerService;
import com.gymMarStrong.service.GymClassService;
import com.gymMarStrong.service.TrainerService;

@Controller
@RequestMapping("/gymClass")
public class GymClassController {

	@Autowired
	private GymClassService gymClassService;
	@Autowired
	private ClassTypeService classTypeService;
	@Autowired
	private TrainerService trainerService;
	@Autowired
	private CustomerService customerService;

	@GetMapping
	public String viewAllGymClasses(ModelMap model) {
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		model.addAttribute("gymClassList", gymClassList);
		model.addAttribute("newGymClass", new GymClass());
		return "gymClassList";
	}

	@GetMapping(value = "/save")
	public String addNewGymClass(ModelMap model) {
		Set<Trainer> trainerList = trainerService.getAllTrainers();
		Set<ClassType> classTypeList = classTypeService.getAllClassTypes();
		model.addAttribute("classTypeList", classTypeList);
		model.addAttribute("trainerList", trainerList);
		model.addAttribute("newGymClass", new GymClass());
		return "gymClassSave";
	}

	@PostMapping(value = "/save")
	public String saveGymClass(@ModelAttribute("newGymClass") GymClass newGymClass, ModelMap model) {
		gymClassService.saveGymClass(newGymClass);
		Set<Trainer> trainerList = trainerService.getAllTrainers();
		Set<ClassType> classTypeList = classTypeService.getAllClassTypes();
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		model.addAttribute("gymClassList", gymClassList);
		model.addAttribute("classTypeList", classTypeList);
		model.addAttribute("trainerList", trainerList);
		model.addAttribute("newGymClass", new GymClass());
		return "gymClassList";
	}

	@PostMapping(value = "/update")
	public String updateGymClass(@RequestParam("gymClassId") String gymClassId, ModelMap model) {
		Set<Trainer> trainerList = trainerService.getAllTrainers();
		Set<ClassType> classTypeList = classTypeService.getAllClassTypes();
		model.addAttribute("newGymClass", gymClassService.getClassById(Integer.valueOf(gymClassId)));
		model.addAttribute("trainerList", trainerList);
		model.addAttribute("classTypeList", classTypeList);
		return "gymClassSave";
	}

	@PostMapping(value = "/delete")
	public String deleteGymClass(@RequestParam("gymClassId") String gymClassId, ModelMap model) {
		gymClassService.deleteGymClassById(Integer.valueOf(gymClassId));
		Set<Trainer> trainerList = trainerService.getAllTrainers();
		Set<ClassType> classTypeList = classTypeService.getAllClassTypes();
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		model.addAttribute("gymClassList", gymClassList);
		model.addAttribute("classTypeList", classTypeList);
		model.addAttribute("trainerList", trainerList);
		model.addAttribute("newGymClass", new GymClass());
		return "gymClassList";
	}

	@PostMapping(value = "/updateParticipants")
	public String updateParticipantsForClass(@RequestParam("gymClassId") String gymClassId, ModelMap model) {
		Set<Customer> enrolledCustomersOfClass = gymClassService.getClassById(Integer.valueOf(gymClassId))
				.getCustomerList();
		Set<Customer> customersNotEnrolledInClass = customerService.getAllCostumers();
		customersNotEnrolledInClass.removeAll(enrolledCustomersOfClass);
		model.addAttribute("enrolledCustomersOfClass", enrolledCustomersOfClass);
		model.addAttribute("customersNotEnrolledInClass", customersNotEnrolledInClass);
		model.addAttribute("gymClassId", gymClassId);
		return "gymClassCustomerManagement";
	}

	@PostMapping(value = "/removeCustomerFromClass")
	public String removeCustomerFromClass(@RequestParam("gymClassId") String gymClassId,
			@RequestParam("customerId") String customerId, ModelMap model) {
		gymClassService.removeCustomerFromClass(Integer.valueOf(gymClassId), Integer.valueOf(customerId));
		Set<Customer> enrolledCustomersOfClass = gymClassService.getClassById(Integer.valueOf(gymClassId))
				.getCustomerList();
		Set<Customer> customersNotEnrolledInClass = customerService.getAllCostumers();
		customersNotEnrolledInClass.removeAll(enrolledCustomersOfClass);
		model.addAttribute("enrolledCustomersOfClass", enrolledCustomersOfClass);
		model.addAttribute("customersNotEnrolledInClass", customersNotEnrolledInClass);
		model.addAttribute("gymClassId", gymClassId);
		return "gymClassCustomerManagement";
	}

	@PostMapping(value = "/addCustomerToClass")
	public String addCustomerToClass(@RequestParam("gymClassId") String gymClassId,
			@RequestParam("customerId") String customerId, ModelMap model) {
		GymClass g = gymClassService.getClassById(Integer.valueOf(gymClassId));
		System.out.println(g.getCustomerList().size());
		gymClassService.addCustomerToClass(Integer.valueOf(gymClassId), Integer.valueOf(customerId));
		Set<Customer> enrolledCustomersOfClass = gymClassService.getClassById(Integer.valueOf(gymClassId))
				.getCustomerList();
		Set<Customer> customersNotEnrolledInClass = customerService.getAllCostumers();
		customersNotEnrolledInClass.removeAll(enrolledCustomersOfClass);
		model.addAttribute("enrolledCustomersOfClass", enrolledCustomersOfClass);
		model.addAttribute("customersNotEnrolledInClass", customersNotEnrolledInClass);
		model.addAttribute("gymClassId", gymClassId);
		return "gymClassCustomerManagement";
		
	}
}
