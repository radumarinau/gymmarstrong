package com.gymMarStrong.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gymMarStrong.hibernate.entityClasses.Customer;
import com.gymMarStrong.hibernate.entityClasses.Subscription;
import com.gymMarStrong.hibernate.entityClasses.SubscriptionType;
import com.gymMarStrong.service.CustomerService;
import com.gymMarStrong.service.SubscriptionService;
import com.gymMarStrong.service.SubscriptionTypeService;

@Controller
@RequestMapping("/subscription")
public class SubscriptionController {

	@Autowired
	private SubscriptionService subscriptionService;
	@Autowired
	private SubscriptionTypeService subscriptionTypeService;
	@Autowired
	private CustomerService customerService;

	@GetMapping
	public String viewAllSubscriptions(ModelMap model) {
		Set<Customer> customerList = customerService.getAllCostumers();
		Set<Subscription> subscriptionList = subscriptionService.getAllSubscriptions();
		Set<SubscriptionType> subscriptionTypeList = subscriptionTypeService.getAllSubscriptionTypes();
		model.addAttribute("subscriptionList", subscriptionList);
		model.addAttribute("subscriptionTypeList", subscriptionTypeList);
		model.addAttribute("customerList", customerList);
		model.addAttribute("newSubscription", new Subscription());
		return "subscriptionList";
	}

	@GetMapping(value = "/save")
	public String addNewSubscription(ModelMap model) {
		Set<Customer> customerList = customerService.getAllCostumers();
		Set<SubscriptionType> subscriptionTypeList = subscriptionTypeService.getAllSubscriptionTypes();
		model.addAttribute("subscriptionTypeList", subscriptionTypeList);
		model.addAttribute("customerList", customerList);
		model.addAttribute("newSubscription", new Subscription());
		return "subscriptionSave";
	}

	@PostMapping(value = "/save")
	public String saveSubscription(@ModelAttribute("newSubscription") Subscription newSubscription, ModelMap model) {
		subscriptionService.saveSubscription(newSubscription);
		Set<Subscription> subscriptionList = subscriptionService.getAllSubscriptions();
		Set<Customer> customerList = customerService.getAllCostumers();
		Set<SubscriptionType> subscriptionTypeList = subscriptionTypeService.getAllSubscriptionTypes();
		model.addAttribute("subscriptionList", subscriptionList);
		model.addAttribute("subscriptionTypeList", subscriptionTypeList);
		model.addAttribute("customerList", customerList);
		model.addAttribute("newSubscription", new Subscription());
		return "subscriptionList";
	}

	@PostMapping(value = "/update")
	public String updateSubscription(@RequestParam("subscriptionId") String subscriptionId, ModelMap model) {
		Set<Customer> customerList = customerService.getAllCostumers();
		Set<SubscriptionType> subscriptionTypeList = subscriptionTypeService.getAllSubscriptionTypes();
		model.addAttribute("subscriptionTypeList", subscriptionTypeList);
		model.addAttribute("customerList", customerList);
		model.addAttribute("newSubscription", subscriptionService.getSubscriptionById(Integer.valueOf(subscriptionId)));
		return "subscriptionSave";
	}

	@PostMapping(value = "/delete")
	public String deleteSubscription(@RequestParam("subscriptionId") String subscriptionId, ModelMap model) {
		subscriptionService.deleteSubscriptionById(Integer.valueOf(subscriptionId));
		Set<Customer> customerList = customerService.getAllCostumers();
		Set<Subscription> subscriptionList = subscriptionService.getAllSubscriptions();
		Set<SubscriptionType> subscriptionTypeList = subscriptionTypeService.getAllSubscriptionTypes();
		model.addAttribute("subscriptionList", subscriptionList);
		model.addAttribute("subscriptionTypeList", subscriptionTypeList);
		model.addAttribute("customerList", customerList);
		model.addAttribute("newSubscription", new Subscription());
		return "subscriptionList";
	}
}
