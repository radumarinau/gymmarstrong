package com.gymMarStrong.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gymMarStrong.hibernate.entityClasses.Customer;
import com.gymMarStrong.hibernate.entityClasses.GymClass;
import com.gymMarStrong.hibernate.entityClasses.Subscription;
import com.gymMarStrong.service.CustomerService;
import com.gymMarStrong.service.GymClassService;
import com.gymMarStrong.service.SubscriptionService;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	@Autowired
	private GymClassService gymClassService;
	@Autowired
	private SubscriptionService subscriptionService;

	@GetMapping
	public String viewAllCustomers(ModelMap model) {
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		Set<Subscription> subscriptionList = subscriptionService.getAllSubscriptions();
		Set<Customer> customerList = customerService.getAllCostumers();
		model.addAttribute("gymClassList", gymClassList);
		model.addAttribute("customerList", customerList);
		model.addAttribute("subscriptionList", subscriptionList);
		model.addAttribute("newCustomer", new Customer());
		return "customerList";
	}

	@GetMapping(value = "/save")
	public String addNewCustomer(ModelMap model) {
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		model.addAttribute("gymClassList", gymClassList);
		model.addAttribute("newCustomer", new Customer());
		return "customerSave";
	}

	@PostMapping(value = "/update")
	public String updateCustomer(@RequestParam("customerId") String customerId, ModelMap model) {
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		Set<Subscription> subscriptionList = subscriptionService.getAllSubscriptions();
		Set<Customer> customerList = customerService.getAllCostumers();
		Customer customer = customerService.getCustomerById(Integer.valueOf(customerId));
		model.addAttribute("gymClassList", gymClassList);
		model.addAttribute("customerList", customerList);
		model.addAttribute("subscriptionList", subscriptionList);
		model.addAttribute("newCustomer", customer);
		return "customerSave";
	}

	@PostMapping(value = "/save")
	public String saveCustomer(@ModelAttribute("newCustomer") Customer newCustomer, ModelMap model) {
		customerService.saveCustomer(newCustomer);
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		Set<Subscription> subscriptionList = subscriptionService.getAllSubscriptions();
		Set<Customer> customerList = customerService.getAllCostumers();
		model.addAttribute("gymClassList", gymClassList);
		model.addAttribute("customerList", customerList);
		model.addAttribute("subscriptionList", subscriptionList);
		model.addAttribute("newCustomer", new Customer());
		return "customerList";
	}

	@PostMapping(value = "/delete")
	public String deleteCustomer(@RequestParam("customerId") String customerId, ModelMap model) {
		customerService.deleteCustomerById(Integer.valueOf(customerId));
		Set<Subscription> subscriptionList = subscriptionService.getAllSubscriptions();
		Set<Customer> customerList = customerService.getAllCostumers();
		Set<GymClass> gymClassList = gymClassService.getAllClasses();
		model.addAttribute("gymClassList", gymClassList);
		model.addAttribute("customerList", customerList);
		model.addAttribute("subscriptionList", subscriptionList);
		model.addAttribute("newCustomer", new Customer());
		return "customerList";
	}

}
