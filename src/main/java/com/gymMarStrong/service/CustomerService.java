package com.gymMarStrong.service;

import java.util.Set;

import com.gymMarStrong.hibernate.entityClasses.Customer;

public interface CustomerService {

	Set<Customer> getAllCostumers();

	Customer getCustomerById(int customerId);

	void deleteCustomerById(int costumerId);

	Customer findCustomer(String firstName, String lastName, Long cnp);

	void saveCustomer(Customer customer);

}