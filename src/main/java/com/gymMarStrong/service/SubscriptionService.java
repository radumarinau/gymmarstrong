package com.gymMarStrong.service;

import java.util.Set;

import com.gymMarStrong.hibernate.entityClasses.Subscription;

public interface SubscriptionService {

	Set<Subscription> getAllSubscriptions();

	Subscription getSubscriptionById(int subscriptionId);

	void deleteSubscriptionById(int subscriptionId);

	void saveSubscription (Subscription subscription);
	
	void deleteSubscriptionsByCustomerId(int customerId);

}
