package com.gymMarStrong.service;

import java.util.Set;

import com.gymMarStrong.hibernate.entityClasses.Trainer;

public interface TrainerService {

	Set<Trainer> getAllTrainers();

	Trainer getTrainerById(int trainerId);

	void deleteTrainerById(int trainerId);

	Trainer findTrainer(String firstName, String lastName, Long cnp);

	void saveTrainer(Trainer trainer);

}
