package com.gymMarStrong.service;

import java.util.Set;

import com.gymMarStrong.hibernate.entityClasses.GymClass;

public interface GymClassService {

	Set<GymClass> getAllClasses();

	GymClass getClassById(int gymClassId);

	void deleteGymClassById(int gymClassId);

	Set<GymClass> getGymClassByClassName(String className);

	void saveGymClass(GymClass gymClass);
	
	void removeCustomerFromClass(int gymClassId, int customerId);
	
	void addCustomerToClass(int gymClassId, int customerId);
}
