package com.gymMarStrong.daoImpl;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gymMarStrong.dao.SubscriptionTypeDAO;
import com.gymMarStrong.hibernate.entityClasses.SubscriptionType;

@Repository
public class SubscriptionTypeDaoImpl implements SubscriptionTypeDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public Set<SubscriptionType> getAllSubscriptionTypes() {
		return new HashSet<SubscriptionType>(
				entityManager.createQuery("Select s from SubscriptionType s").getResultList());
	}

	@Override
	public SubscriptionType getSubscriptionTypeById(int subscriptionTypeId) {
		return entityManager.find(SubscriptionType.class, subscriptionTypeId);
	}

	@Override
	public void deleteSubscriptionTypeById(int subscriptionTypeId) {
		entityManager.createQuery("Delete from SubscriptionType s where s.subscriptionTypeId=:subscriptionTypeId ")
				.setParameter("subscriptionTypeId", subscriptionTypeId).executeUpdate();
		System.out.println("Subscription type with id=" + subscriptionTypeId + "was deleted succesfully.");

	}

	@Override
	public void saveSubscriptionType(SubscriptionType subscriptionType) {
		entityManager.persist(subscriptionType);

	}
}