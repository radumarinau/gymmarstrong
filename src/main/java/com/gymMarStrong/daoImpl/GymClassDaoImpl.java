package com.gymMarStrong.daoImpl;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gymMarStrong.dao.GymClassDAO;
import com.gymMarStrong.hibernate.entityClasses.GymClass;

@Repository
public class GymClassDaoImpl implements GymClassDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public Set<GymClass> getAllClasses() {
		return new HashSet<GymClass>(entityManager.createQuery("Select g from GymClass g").getResultList());
	}

	@Override
	public GymClass getClassById(int gymClassId) {
		return entityManager.find(GymClass.class, gymClassId);
	}

	@Override
	public void deleteGymClassById(int gymClassId) {
		entityManager.createQuery("Delete from GymClass g where g.gymClassId=:gymClassId ")
				.setParameter("gymClassId", gymClassId).executeUpdate();
		System.out.println("GymClass with id=" + gymClassId + "was deleted succesfully.");

	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<GymClass> getGymClassByClassName(String gymClassName) {
		return new HashSet<GymClass>(
				entityManager.createQuery("Select from GymClass g where g.gymClassName=:gymClassName")
						.setParameter("gymClassName", gymClassName).getResultList());
	}

	@Override
	public void saveGymClass(GymClass gymClass) {
		entityManager.merge(gymClass);
	}

}
