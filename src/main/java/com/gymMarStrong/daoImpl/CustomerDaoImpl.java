package com.gymMarStrong.daoImpl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.gymMarStrong.dao.CustomerDAO;
import com.gymMarStrong.hibernate.entityClasses.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public Set<Customer> getAllCostumers() {
		Query query = entityManager.createQuery("Select c from Customer c");
		Set<Customer> allCustomers = new HashSet<Customer>(query.getResultList());
		return allCustomers;
	}

	@Override
	public Customer getCustomerById(int customerId) {
		return entityManager.find(Customer.class, customerId);
	}

	@Override
	public void deleteCustomerById(int customerId) {
		entityManager.createQuery("Delete from Customer c where c.customerId=:customerId ")
				.setParameter("customerId", customerId).executeUpdate();
		System.out.println("Customer with id=" + customerId + " deleted succesfully.");
	}

	@Override
	public Customer findCustomer(String firstName, String lastName, Long cnp) {
		return (Customer) entityManager
				.createQuery(
						"Select from Customer c where c.firstName=:firstName And c.lastName=:lastName And c.cnp=:cnp")
				.setParameter("firstName", firstName).setParameter("lastName", lastName).setParameter("cnp", cnp)
				.getSingleResult();
	}

	@Override
	public void saveCustomer(Customer customer) {
		entityManager.merge(customer);
	}
}
