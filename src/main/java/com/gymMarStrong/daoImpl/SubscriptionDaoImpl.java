package com.gymMarStrong.daoImpl;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gymMarStrong.dao.SubscriptionDAO;
import com.gymMarStrong.hibernate.entityClasses.Subscription;

@Repository
public class SubscriptionDaoImpl implements SubscriptionDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public Set<Subscription> getAllSubscriptions() {
		return new HashSet<Subscription>(entityManager.createQuery("Select s from Subscription s").getResultList());
	}

	@Override
	public Subscription getSubscriptionById(int subscriptionId) {
		return entityManager.find(Subscription.class, subscriptionId);
	}

	@Override
	public void deleteSubscriptionById(int subscriptionId) {
		entityManager.createQuery("Delete from Subscription s where s.subscriptionId=:subscriptionId ")
				.setParameter("subscriptionId", subscriptionId).executeUpdate();
		System.out.println("Subscription with id=" + subscriptionId + "was deleted succesfully.");

	}

	@Override
	public void saveSubscription(Subscription subscription) {
		entityManager.persist(subscription);

	}

	@Override
	public void deleteSubscriptionsByCustomerId(int customerId) {
		/*
		 * We select the subscription to be deleted using s.customer.customerId. A
		 * subscription has an inner customer and that customer has an Id (exactly like
		 * in the POJO)
		 */
		entityManager.createQuery("Delete from Subscription s where s.customer.customerId=:customerId ")
				.setParameter("customerId", customerId).executeUpdate();

	}

}
