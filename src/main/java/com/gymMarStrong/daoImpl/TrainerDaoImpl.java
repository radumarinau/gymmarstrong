package com.gymMarStrong.daoImpl;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.gymMarStrong.dao.TrainerDAO;
import com.gymMarStrong.hibernate.entityClasses.Trainer;

@Repository
public class TrainerDaoImpl implements TrainerDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public Set<Trainer> getAllTrainers() {
		return new HashSet<Trainer>(entityManager.createQuery("Select t from Trainer t").getResultList());
	}

	@Override
	public Trainer getTrainerById(int trainerId) {
		return entityManager.find(Trainer.class, trainerId);
	}

	@Override
	public void deleteTrainerById(int trainerId) {
		entityManager.createQuery("Delete from Trainer t where t.trainerId=:trainerId ")
				.setParameter("trainerId", trainerId).executeUpdate();

		System.out.println("Trainer with id=" + trainerId + "was deleted succesfully.");

	}

	@Override
	public Trainer findTrainer(String firstName, String lastName, Long cnp) {
		return (Trainer) entityManager
				.createQuery(
						"Select from Trainer t where t.firstName=:firstName And t.lastName=:lastName And t.cnp=:cnp")
				.setParameter("firstName", firstName).setParameter("lastName", lastName).setParameter("cnp", cnp)
				.getSingleResult();
	}

	@Override
	public void saveTrainer(Trainer trainer) {
		entityManager.persist(trainer);

	}

}
