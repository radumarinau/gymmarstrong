package com.gymMarStrong.daoImpl;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.gymMarStrong.dao.ClassTypeDAO;
import com.gymMarStrong.hibernate.entityClasses.ClassType;

@Repository
public class ClassTypeDaoImpl implements ClassTypeDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public Set<ClassType> getAllClassTypes() {
		Query query = entityManager.createQuery("Select c from ClassType c");
		Set<ClassType> allClassTypes = new HashSet<ClassType>(query.getResultList());
		return allClassTypes;
	}

	@Override
	public ClassType getClassTypeById(int classTypeId) {
		return entityManager.find(ClassType.class, classTypeId);
	}

	@Override
	public void deleteClassTypeById(int classTypeId) {
		entityManager.createQuery("Delete from ClassType c where c.classTypeId=:classTypeId ")
				.setParameter("classTypeId", classTypeId)
				.executeUpdate();
		System.out.println("ClassType with id=" + classTypeId + "was deleted succesfully.");
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<ClassType> findClassTypeByName(String classTypeName) {
		return new HashSet<ClassType>(entityManager.createQuery("Select from ClassType c where c.classTypeName=:classTypeName ")
				.setParameter("classTypeName", classTypeName)
				.getResultList());
	}

	@Override
	public void saveClassType(ClassType classType) {
		entityManager.persist(classType);

	}

}
