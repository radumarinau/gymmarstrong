package com.gymMarStrong.serviceImpl;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gymMarStrong.dao.CustomerDAO;
import com.gymMarStrong.dao.GymClassDAO;
import com.gymMarStrong.dao.SubscriptionDAO;
import com.gymMarStrong.hibernate.entityClasses.Customer;
import com.gymMarStrong.service.CustomerService;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDAO customerDao;
	
	@Autowired 
	SubscriptionDAO subscriptionDao;
	
	@Autowired
	GymClassDAO gymClassDao;

	@Override
	public Set<Customer> getAllCostumers() {
		return customerDao.getAllCostumers();
	}

	@Override
	public Customer getCustomerById(int customerId) {
		return customerDao.getCustomerById(customerId);
	}

	@Override
	public void deleteCustomerById(int customerId) {
		subscriptionDao.deleteSubscriptionsByCustomerId(customerId);
		customerDao.deleteCustomerById(customerId);

	}

	@Override
	public Customer findCustomer(String firstName, String lastName, Long cnp) {
		return customerDao.findCustomer(firstName, lastName, cnp);
	}

	@Override
	public void saveCustomer(Customer customer) {
		customerDao.saveCustomer(customer);
	}

	
}
