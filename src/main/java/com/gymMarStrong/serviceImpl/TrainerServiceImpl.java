package com.gymMarStrong.serviceImpl;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gymMarStrong.dao.TrainerDAO;
import com.gymMarStrong.hibernate.entityClasses.Trainer;
import com.gymMarStrong.service.TrainerService;

@Service
@Transactional
public class TrainerServiceImpl implements TrainerService {

	@Autowired
	TrainerDAO trainerDao;

	@Override
	public Set<Trainer> getAllTrainers() {
		return trainerDao.getAllTrainers();
	}

	@Override
	public Trainer getTrainerById(int trainerId) {
		return trainerDao.getTrainerById(trainerId);
	}

	@Override
	public void deleteTrainerById(int trainerId) {
		trainerDao.deleteTrainerById(trainerId);

	}

	@Override
	public Trainer findTrainer(String firstName, String lastName, Long cnp) {
		return trainerDao.findTrainer(firstName, lastName, cnp);
	}

	@Override
	public void saveTrainer(Trainer trainer) {
		trainerDao.saveTrainer(trainer);

	}

}
