package com.gymMarStrong.serviceImpl;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gymMarStrong.dao.SubscriptionTypeDAO;
import com.gymMarStrong.hibernate.entityClasses.SubscriptionType;
import com.gymMarStrong.service.SubscriptionTypeService;

@Service
@Transactional
public class SubscriptionTypeServiceImpl implements SubscriptionTypeService {

	@Autowired
	SubscriptionTypeDAO subscriptionTypeDao;

	@Override
	public Set<SubscriptionType> getAllSubscriptionTypes() {
		return subscriptionTypeDao.getAllSubscriptionTypes();
	}

	@Override
	public SubscriptionType getSubscriptionTypeById(int subscriptionTypeId) {
		return subscriptionTypeDao.getSubscriptionTypeById(subscriptionTypeId);
	}

	@Override
	public void deleteSubscriptionTypeById(int subscriptionTypeId) {
		subscriptionTypeDao.deleteSubscriptionTypeById(subscriptionTypeId);

	}

	@Override
	public void saveSubscriptionType(SubscriptionType subscriptionType) {
		subscriptionTypeDao.saveSubscriptionType(subscriptionType);

	}

}
