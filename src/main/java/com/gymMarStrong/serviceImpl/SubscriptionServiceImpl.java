package com.gymMarStrong.serviceImpl;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gymMarStrong.dao.SubscriptionDAO;
import com.gymMarStrong.hibernate.entityClasses.Subscription;
import com.gymMarStrong.service.SubscriptionService;

@Service
@Transactional
public class SubscriptionServiceImpl implements SubscriptionService {

	@Autowired
	SubscriptionDAO subscriptionDao;

	@Override
	public Set<Subscription> getAllSubscriptions() {
		return subscriptionDao.getAllSubscriptions();
	}

	@Override
	public Subscription getSubscriptionById(int subscriptionId) {
		return subscriptionDao.getSubscriptionById(subscriptionId);
	}

	@Override
	public void deleteSubscriptionById(int subscriptionId) {
		subscriptionDao.deleteSubscriptionById(subscriptionId);

	}

	@Override
	public void saveSubscription(Subscription subscription) {
		subscriptionDao.saveSubscription(subscription);
	}

	@Override
	public void deleteSubscriptionsByCustomerId(int customerId) {
		subscriptionDao.deleteSubscriptionsByCustomerId(customerId);

	}

}
