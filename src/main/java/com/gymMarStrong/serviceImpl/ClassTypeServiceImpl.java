package com.gymMarStrong.serviceImpl;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gymMarStrong.dao.ClassTypeDAO;
import com.gymMarStrong.hibernate.entityClasses.ClassType;
import com.gymMarStrong.service.ClassTypeService;

@Service
@Transactional
public class ClassTypeServiceImpl implements ClassTypeService {

	@Autowired
	ClassTypeDAO classTypeDao;

	@Override
	public Set<ClassType> getAllClassTypes() {
		return classTypeDao.getAllClassTypes();
	}

	@Override
	public ClassType getClassTypeById(int classTypeId) {
		return classTypeDao.getClassTypeById(classTypeId);
	}

	@Override
	public void deleteClassTypeById(int classTypeId) {
		classTypeDao.deleteClassTypeById(classTypeId);
	}

	@Override
	public Set<ClassType> findClassTypeByName(String classTypeName) {
		return classTypeDao.findClassTypeByName(classTypeName);
	}

	@Override
	public void saveClassType(ClassType ClassType) {
		classTypeDao.saveClassType(ClassType);

	}

}
