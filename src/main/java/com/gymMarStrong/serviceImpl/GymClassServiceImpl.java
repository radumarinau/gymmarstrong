package com.gymMarStrong.serviceImpl;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gymMarStrong.dao.GymClassDAO;
import com.gymMarStrong.hibernate.entityClasses.Customer;
import com.gymMarStrong.hibernate.entityClasses.GymClass;
import com.gymMarStrong.service.CustomerService;
import com.gymMarStrong.service.GymClassService;

@Service
@Transactional
public class GymClassServiceImpl implements GymClassService {

	@Autowired
	GymClassDAO gymClassDao;
	@Autowired
	CustomerService customerService;

	@Override
	public Set<GymClass> getAllClasses() {
		return gymClassDao.getAllClasses();
	}

	@Override
	public GymClass getClassById(int gymClassId) {
		return gymClassDao.getClassById(gymClassId);
	}

	@Override
	public void deleteGymClassById(int gymClassId) {
		gymClassDao.deleteGymClassById(gymClassId);

	}

	@Override
	public Set<GymClass> getGymClassByClassName(String className) {
		return gymClassDao.getGymClassByClassName(className);
	}

	@Override
	public void saveGymClass(GymClass gymClass) {
		gymClassDao.saveGymClass(gymClass);

	}

	@Override
	public void removeCustomerFromClass(int gymClassId, int customerId) {
		Customer customer = customerService.getCustomerById(customerId);
		customer.getGymClassList().removeIf(g -> g.getGymClassId().equals(gymClassId));
		customerService.saveCustomer(customer);
	}

	@Override
	public void addCustomerToClass(int gymClassId, int customerId) {
		Customer customer = customerService.getCustomerById(customerId);
		GymClass gymClass = gymClassDao.getClassById(gymClassId);
		customer.getGymClassList().add(gymClass);
		customerService.saveCustomer(customer);
	}

}
