package com.gymMarStrong.hibernate.entityClasses;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "gymclass")
public class GymClass {

    @Id
    @Column(name = "gymClassId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer gymClassId;

    @Column(name = "startDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDateTime;

    @Column(name = "endDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDateTime;

    @Column(name = "maxParticipants")
    private int maxParticipants;

    @ManyToOne
    @JoinColumn(name = "classTypeId")
    private ClassType classType;

    @ManyToOne
    @JoinColumn(name = "trainerId")
    private Trainer trainer;

    @ManyToMany(mappedBy = "gymClassList", fetch = FetchType.EAGER)
    private Set<Customer> customerList;

    public GymClass() {
        super();
    }

    public Integer getGymClassId() {
        return gymClassId;
    }

    public void setGymClassId(Integer gymClassId) {
        this.gymClassId = gymClassId;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setNoOfParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public ClassType getClassType() {
        return classType;
    }

    public void setClassType(ClassType classType) {
        this.classType = classType;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public Set<Customer> getParticipants() {
        return customerList;
    }

    public void setParticipants(Set<Customer> participants) {
        this.customerList = participants;
    }

    public Set<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(Set<Customer> customerList) {
        this.customerList = customerList;
    }

    public String getInterval() {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(this.startDateTime) + " / " + formatter.format(this.endDateTime);
    }

    @Override
    public String toString() {
        return this.classType.getClassTypeName() + " between " + this.startDateTime + " " + this.endDateTime;
    }
    
    public int getNumberOfClassParticiapants() {
    	return customerList.size();
    }

}