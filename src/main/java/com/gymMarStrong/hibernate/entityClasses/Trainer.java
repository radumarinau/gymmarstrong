package com.gymMarStrong.hibernate.entityClasses;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "trainer")
public class Trainer {

	@Id
	@Column(name = "trainerId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer trainerId;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "cnp")
	private Long cnp;

	@OneToMany(mappedBy = "trainer", fetch = FetchType.EAGER)
	private Set<GymClass> gymClassList;

	public Trainer() {
		super();
	}

	public Integer getTrainerId() {
		return trainerId;
	}

	public void setTrainerId(Integer trainerId) {
		this.trainerId = trainerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getCnp() {
		return cnp;
	}

	public void setCnp(Long cnp) {
		this.cnp = cnp;
	}

	public Set<GymClass> getGymClassList() {
		return gymClassList;
	}

	public void setGymClassList(Set<GymClass> gymClassList) {
		this.gymClassList = gymClassList;
	}

	public String getFullName() {
		return firstName + " " + lastName;
	}

	@Override
	public String toString() {
		return "Id:" + trainerId + " Fn:" + firstName + " Ln:" + lastName;
	}

}
