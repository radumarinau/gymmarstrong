package com.gymMarStrong.hibernate.entityClasses;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer {
	@Id
	@Column(name = "customerId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer customerId;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "cnp")
	private Long cnp;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "customersgymclasses", joinColumns = { @JoinColumn(name = "customerId") }, inverseJoinColumns = {
			@JoinColumn(name = "gymClassId") })
	private Set<GymClass> gymClassList;

	@OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
	private Set<Subscription> subscriptionList;

	public Customer() {
		super();
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getCnp() {
		return cnp;
	}

	public void setCnp(Long cnp) {
		this.cnp = cnp;
	}

	public Set<GymClass> getGymClassList() {
		return gymClassList;
	}

	public void setGymClassList(Set<GymClass> gymClassList) {
		this.gymClassList = gymClassList;
	}

	public Set<Subscription> getSubscriptionList() {
		return subscriptionList;
	}

	public void setSubscriptionList(Set<Subscription> subscriptionList) {
		this.subscriptionList = subscriptionList;
	}

	public String getFullName() {
		return firstName + " " + lastName;
	}
	
	public void setFullName(String fullName) {
		fullName = fullName.trim();
		String[] names = fullName.split(" ");
		this.firstName = names[0];
		this.lastName = names[1];
	}
	
	public String getCredentials() {
		return "Name: " + getFullName() + " CNP:" + String.valueOf(cnp);
	}

	public String toString() {
		return "Id:" + customerId + " Fn:" + firstName + " Ln:" + lastName;
	}

}
