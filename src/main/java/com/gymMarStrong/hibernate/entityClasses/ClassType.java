package com.gymMarStrong.hibernate.entityClasses;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "classtype")
public class ClassType {

	@Id
	@Column(name = "classTypeId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer classTypeId;

	@Column(name = "classTypeName")
	private String classTypeName;

	@Column(name = "classTypeDescription")
	private String classTypeDescription;

	public ClassType() {
		super();
	}

	public Integer getClassTypeId() {
		return classTypeId;
	}

	public void setClassTypeId(Integer classTypeId) {
		this.classTypeId = classTypeId;
	}

	public String getClassTypeName() {
		return classTypeName;
	}

	public void setClassTypeName(String classTypeName) {
		this.classTypeName = classTypeName;
	}

	public String getClassTypeDescription() {
		return classTypeDescription;
	}

	public void setClassTypeDescription(String classTypeDescription) {
		this.classTypeDescription = classTypeDescription;
	}
	
	

	@Override
	public String toString() {
		return this.classTypeName + " - " + this.classTypeDescription;
	}

	
}
