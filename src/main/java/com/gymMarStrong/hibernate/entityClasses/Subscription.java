package com.gymMarStrong.hibernate.entityClasses;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "subscription")
public class Subscription {

	@Id
	@Column(name = "subscriptionId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer subscriptionId;

	@Column(name = "startDate")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startDateTime;

	@Column(name = "endDate")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endDateTime;

	@Column(name = "price")
	private int price;

	@ManyToOne
	@JoinColumn(name = "customerId")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "subscriptionTypeId")
	private SubscriptionType subscriptionType;

	public Subscription() {
		super();
	}

	public Integer getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Integer subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	@Override
	public String toString() {
		return this.subscriptionType.getServices() + "- between " + this.startDateTime + " - " + this.endDateTime;  
	}

	
}
