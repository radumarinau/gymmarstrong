package com.gymMarStrong.hibernate.entityClasses;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subscriptiontype")
public class SubscriptionType {

	@Id
	@Column(name = "subscriptionTypeId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer subscriptionTypeId;

	@Column(name = "services")
	private String services;

	@Column(name = "accessTime")
	private String accessTime;

	public SubscriptionType() {
		super();
	}

	public Integer getSubscriptionTypeId() {
		return subscriptionTypeId;
	}

	public void setSubscriptionTypeId(Integer subscriptionTypeId) {
		this.subscriptionTypeId = subscriptionTypeId;
	}

	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	public String getAccessTime() {
		return accessTime;
	}

	public void setAccessTime(String accessTime) {
		this.accessTime = accessTime;
	}

	
}
