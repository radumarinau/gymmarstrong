package com.gymMarStrong.dao;

import java.util.Set;

import com.gymMarStrong.hibernate.entityClasses.Customer;

public interface CustomerDAO {

	Set<Customer> getAllCostumers();

	Customer getCustomerById(int customerId);

	void deleteCustomerById(int costumerId);

	Customer findCustomer(String firstName, String lastName, Long cnp);

	void saveCustomer(Customer customer);

}