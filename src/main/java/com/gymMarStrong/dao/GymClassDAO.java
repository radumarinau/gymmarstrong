package com.gymMarStrong.dao;

import java.util.Set;

import com.gymMarStrong.hibernate.entityClasses.GymClass;

public interface GymClassDAO {

	Set<GymClass> getAllClasses();

	GymClass getClassById(int gymClassId);

	void deleteGymClassById(int gymClassId);

	Set<GymClass> getGymClassByClassName(String className);

	void saveGymClass(GymClass gymClass);

}
