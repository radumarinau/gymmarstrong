package com.gymMarStrong.dao;

import java.util.Set;

import com.gymMarStrong.hibernate.entityClasses.SubscriptionType;

public interface SubscriptionTypeDAO {

	Set<SubscriptionType> getAllSubscriptionTypes();

	SubscriptionType getSubscriptionTypeById(int subscriptionTypeId);

	void deleteSubscriptionTypeById(int subscriptionTypeId);

	void saveSubscriptionType(SubscriptionType subscriptionType);
}
