package com.gymMarStrong.dao;

import java.util.Set;

import com.gymMarStrong.hibernate.entityClasses.Trainer;

public interface TrainerDAO {

	Set<Trainer> getAllTrainers();

	Trainer getTrainerById(int trainerId);

	void deleteTrainerById(int trainerId);

	Trainer findTrainer(String firstName, String lastName, Long cnp);

	void saveTrainer(Trainer trainer);

}
