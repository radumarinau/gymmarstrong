package com.gymMarStrong.dao;

import java.util.Set;

import com.gymMarStrong.hibernate.entityClasses.ClassType;

public interface ClassTypeDAO {

	Set<ClassType> getAllClassTypes();

	ClassType getClassTypeById(int classTypeId);

	void deleteClassTypeById(int classTypeId);

	Set<ClassType> findClassTypeByName(String classTypeName);

	void saveClassType(ClassType ClassType);
}
