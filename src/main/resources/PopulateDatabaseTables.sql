INSERT INTO `classtype` VALUES (5,'Cycling','People riding bicycles'),(6,'TRX','People sweating a lot'),(7,'Bodycombat','Learning self defense');

INSERT INTO `customer` VALUES (5,'Radu','Marinau',4444444444444),(6,'George','Miclea',5555555555555),(7,'Ana','Bratu',6666666666666),(8,'Ramona','Ciurila',9999999999999);

INSERT INTO `trainer` VALUES (2,'Bruce','Lee',1111111111111),(3,'Arnold','Schwarzenegger',2222222222222),(4,'Jason','Statham',3333333333333);

INSERT INTO `gymclass` VALUES (2,'2018-04-30 21:00:00','2018-05-30 21:00:00',31,7,2),(3,'2018-04-30 21:00:00','2018-05-14 21:00:00',5,5,3),(4,'2018-05-14 21:00:00','2018-05-30 21:00:00',10,6,3);

INSERT INTO `customersgymclasses` VALUES (1,5,3),(2,6,3),(3,6,4);

INSERT INTO `subscriptiontype` VALUES (4,'Full access','08-20'),(5,'Pool access','08-16'),(6,'Powerlifting access','8-16'),(7,'Sauna access','18-22');

INSERT INTO `subscription` VALUES (2,'2017-04-30','2017-05-30',150.00,5,4),(3,'2018-04-30','2018-05-30',100.00,5,7),(4,'2018-03-31','2018-04-29',100.00,7,5),(5,'2018-04-30','2018-05-30',150.00,6,6);