CREATE SCHEMA `gymmarstrong`;
USE gymmarstrong;

CREATE TABLE `classtype` (
  `classTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `classTypeName` varchar(50) NOT NULL,
  `classTypeDescription` varchar(255) NOT NULL,
  PRIMARY KEY (`classTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `customer` (
  `customerId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(25) NOT NULL,
  `lastName` varchar(25) NOT NULL,
  `cnp` bigint(13) NOT NULL,
  PRIMARY KEY (`customerId`),
  UNIQUE KEY `cnp_UNIQUE` (`cnp`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `trainer` (
  `trainerId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `cnp` bigint(13) NOT NULL,
  PRIMARY KEY (`trainerId`),
  UNIQUE KEY `cnp_UNIQUE` (`cnp`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `gymclass` (
  `gymClassId` int(11) NOT NULL AUTO_INCREMENT,
  `startDate` datetime NOT NULL,
  `endDate` datetime NOT NULL,
  `nrOfParticipants` int(11) NOT NULL,
  `classTypeId` int(11) NOT NULL,
  `trainerId` int(11) NOT NULL,
  PRIMARY KEY (`gymClassId`),
  KEY `gymclass_trainer_idx` (`trainerId`),
  KEY `gymclass_classtype_idx` (`classTypeId`),
  CONSTRAINT `gymclass_classtype` FOREIGN KEY (`classTypeId`) REFERENCES `classtype` (`classTypeId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `gymclass_trainer` FOREIGN KEY (`trainerId`) REFERENCES `trainer` (`trainerId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `customersgymclasses` (
  `customersGymClassesId` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `gymClassId` int(11) NOT NULL,
  PRIMARY KEY (`customersGymClassesId`),
  KEY `customersgymclasses` (`customerId`),
  KEY `customersgymclasses_classes_idx` (`gymClassId`),
  CONSTRAINT `customersgymclasses_classes` FOREIGN KEY (`gymClassId`) REFERENCES `gymclass` (`gymClassId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `customersgymclasses_customer` FOREIGN KEY (`customerId`) REFERENCES `customer` (`customerId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `subscriptiontype` (
  `subscriptionTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `services` varchar(255) NOT NULL,
  `accessTime` varchar(150) NOT NULL,
  PRIMARY KEY (`subscriptionTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `subscription` (
  `subscriptionId` int(11) NOT NULL AUTO_INCREMENT,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `price` float(6,2) NOT NULL,
  `customerId` int(11) NOT NULL,
  `subscriptionTypeId` int(11) NOT NULL,
  PRIMARY KEY (`subscriptionId`),
  KEY `subscription_subscriptiontype_idx` (`subscriptionTypeId`),
  KEY `subscription_customer_idx` (`customerId`),
  CONSTRAINT `subscription_customer` FOREIGN KEY (`customerId`) REFERENCES `customer` (`customerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `subscription_subscriptiontype` FOREIGN KEY (`subscriptionTypeId`) REFERENCES `subscriptiontype` (`subscriptionTypeId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;